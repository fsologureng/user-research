# Introduction

Applying the method described in [A Beg­inner’s Guide to Finding User Needs](https://jdittrich.github.io/userNeedResearchBook/), the goal is to collect evidence that opening issues on Forgejo is made more difficult than it should be because of accessibility issues.

# [Questions](https://jdittrich.github.io/userNeedResearchBook/#writing-a-research-project-question)

* "How and why people experience difficulties opening issues on Forgejo because of accessibility issues?"

# [People](https://jdittrich.github.io/userNeedResearchBook/#find-people-who-participate-in-your-research)

* ???
